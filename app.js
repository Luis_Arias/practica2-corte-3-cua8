const express = require('express');
const router = require('./router/router');
const app = express();

app.set('viewn engine', 'ejs');

app.engine("html", require("ejs").renderFile);
app.use(router);

app.use('/', require("./router/router"));

app.listen(5000, ()=>{
    console.log('Puerto encendido')
});