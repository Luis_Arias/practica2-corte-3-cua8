const mysql2 = require('mysql2');

const conexion = mysql2.createConnection({
    host: 'localhost',
    user: 'root',
    password:'',
    database:'alumnospartes'
})

conexion.connect((error)=>{
    if(error){
        console.error('El error de conexión es:' + error);
        return
    }
    console.log('Conexión exitosa');
})

module.exports = conexion